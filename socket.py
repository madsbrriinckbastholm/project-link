from flask import Flask
# from flask_sockets import Sockets
import asyncio
from flask_sock import Sock

app = Flask(__name__)
sock = Sock(app)

@sock.route('/echo')
def echo(ws):
    while True:
        data = ws.receive()
        ws.send(str(data))

if __name__ == "__main__":
    sock.init_app(app)
    app.run(port=3000)
